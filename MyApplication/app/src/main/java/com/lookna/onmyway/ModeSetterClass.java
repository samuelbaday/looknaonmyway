package com.lookna.onmyway;

/**
 * Created by desktop1 on 15 May 2017.
 */
public class ModeSetterClass {
    public static int DISTANCE = 50;
    private static int intervalTime = 10000;
    private static int modeType = 0;

    public static final int NO_TRIP = 0;
    public static final int WITH_TRIP = 1;
    public static final int NO_TRIP_INTERVAL = 10000;
    public static final int WITH_TRIP_INTERVAL = 4000;

    public ModeSetterClass(){}

    public ModeSetterClass(int dist, int interval, int mode){
            setDistance(dist);
            setIntervalTime(interval);
            setModeType(mode);
    }

    public void setDistance(int dist){
        DISTANCE = dist;
    }
    public void setIntervalTime(int interval){
        intervalTime = interval;
    }
    public void setModeType(int mode){
        modeType = mode;
    }

    public static int getIntervalTime(){
        return intervalTime;
    }
    public static int getModeType(){
        return modeType;
    }
}
