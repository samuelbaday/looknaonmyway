package com.lookna.onmyway;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.MainThread;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OnMyWayMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback {

    @Bind(R.id.my_location_button)
    ImageView myLocationButton;

    private GoogleMap mMap;
    private final int LOCATION_TAG = 1112;
    private final int REQUEST_PERMISSION_SETTING = 99;
    private Marker mCurrLocationMarker;
    private Marker mTargetLocationMarker;
    private static boolean cameraUpdate = true;
    private static LatLng updateLatLng;
    private final int MAP_ZOOM_LEVEL = 16;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 92;
    private final String PLACE_TAG = "PLACE_TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_my_way_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        cameraUpdate = true;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_set_trip);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                findPlace();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onDestroy() {
        destroyInputterService();
        super.onDestroy();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.on_my_way_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {

        }  else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        boolean success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.map_style));
        if (!success) {
            Log.e("MAP STYLE: ", "Style parsing failed.");
        }

        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setRotateGesturesEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setMapToolbarEnabled(false);
        mUiSettings.setIndoorLevelPickerEnabled(true);

        buildGoogleConnection();
        localbroadcastSetup();
    }


    @MainThread
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = OnMyWayMain.this
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;
        } else {
            return false;
        }
    }

    @MainThread
    protected void alertbox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(OnMyWayMain.this, R.style.alertbox_dialog));
        builder.setMessage("Your Device's GPS is Disabled")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Turn On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void buildGoogleConnection(){
        Boolean flag = OnMyWayMain.this.displayGpsStatus();
        if (flag) {
            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET},LOCATION_TAG);
            } else {
                startInputterServiceNoTrip();
            }
            if(Build.VERSION.SDK_INT >= 23){
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION)){
                    ActivityCompat.requestPermissions(OnMyWayMain.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, LOCATION_TAG);
                } else {
                    startInputterServiceNoTrip();
                }
            }
        } else {
            alertbox();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_TAG) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startInputterServiceNoTrip();
            } else {
                // for each permission check if the user granted/denied them
                // you may want to group the rationale in a single dialog,
                // this is just an example
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = false;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            showRationale = shouldShowRequestPermissionRationale(permission);
                        }
                        if (!showRationale) {
                            // user also CHECKED "never ask again"
                            // you can either enable some fall back,
                            // disable features of your app
                            // or open another dialog explaining
                            // again the permission and directing to
                            // the app setting
                            manualPermission("",getString(R.string.manual_permission_location));

                        } else if (android.Manifest.permission.ACCESS_FINE_LOCATION.equals(permission)) {
                            showRationale(permission, getString(R.string.location_denied));
                            // user did NOT check "never ask again"
                            // this is a good place to explain the user
                            // why you need the permission and ask if he wants
                            // to accept it (the rationale)
                        } else if (false) {
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(PLACE_TAG, "Place: " + place.getName());

                if(mTargetLocationMarker != null) {
                    if (mTargetLocationMarker.isVisible()) {
                        mTargetLocationMarker.remove();
                    }
                }

                mTargetLocationMarker = mMap.addMarker(new MarkerOptions()
                        .title(place.getName().toString())
                        .position(place.getLatLng())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker))
                        .snippet(place.getAddress().toString()));

                mTargetLocationMarker.showInfoWindow();

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), MAP_ZOOM_LEVEL);
                mMap.animateCamera(cameraUpdate);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(PLACE_TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    protected void showRationale(String s,String s2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(OnMyWayMain.this, R.style.alertbox_dialog));
        builder.setMessage(s2)
                .setCancelable(false)
                .setTitle("** IMPORTANT PERMISSIONS **")
                .setPositiveButton("I'm sure",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                            }
                        })
                .setNegativeButton("Re-try",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
                                if(Build.VERSION.SDK_INT >= 23){
                                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)){
                                        ActivityCompat.requestPermissions(OnMyWayMain.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_TAG);
                                    }
                                }
                                dialog.dismiss();
                            }
                        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    protected void manualPermission(String s,String s2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(OnMyWayMain.this, R.style.alertbox_dialog));
        builder.setMessage(s2)
                .setCancelable(false)
                .setTitle("ACCEPT PERMISSIONS MANUALLY")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);

                                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);

                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.dismiss();
                            }
                        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void localbroadcastSetup(){
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(getString(R.string.broadcast_filter)));

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("MESSAGE");
            Log.d(getString(R.string.broadcast_tag), "MESSAGE RECEIVED: " + message);
            if(message.contains(getString(R.string.broadcast_message))){
                Double lat1 = 0.0,lat2 = 0.0;
                updateMainMapFunction(intent.getDoubleExtra(getString(R.string.broadcast_lat),lat1),intent.getDoubleExtra(getString(R.string.broadcast_lng),lat2));
            }
        }
    };

    public void startInputterServiceNoTrip(){
        Intent serviceIntent = new Intent(OnMyWayMain.this, LocationInputterService.class);
        new ModeSetterClass(ModeSetterClass.DISTANCE,ModeSetterClass.NO_TRIP_INTERVAL,ModeSetterClass.NO_TRIP);
        startService(serviceIntent);
    }

    public void startInputterServiceWithTrip(){
        Intent serviceIntent = new Intent(OnMyWayMain.this, LocationInputterService.class);
        new ModeSetterClass(ModeSetterClass.DISTANCE,ModeSetterClass.WITH_TRIP_INTERVAL,ModeSetterClass.WITH_TRIP);
        startService(serviceIntent);
    }

    public void destroyInputterService(){
        Intent serviceIntent = new Intent(OnMyWayMain.this, LocationInputterService.class);
        stopService(serviceIntent);
    }

    public void updateMainMapFunction(Double lat,Double lng){
        updateLatLng = new LatLng(lat, lng);
        if(cameraUpdate) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(updateLatLng, MAP_ZOOM_LEVEL);
            mMap.moveCamera(cameraUpdate);
            this.cameraUpdate = false;
            setMyLocationButton();
        }


        if(mCurrLocationMarker != null) {
            if (mCurrLocationMarker.isVisible()) {
                mCurrLocationMarker.remove();
            }
        }


        mCurrLocationMarker = mMap.addMarker(new MarkerOptions()
                .title("YOU ARE HERE")
                .position(updateLatLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker))
                .snippet("Welcome to Lookna"));

    }

    public void setMyLocationButton(){
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(updateLatLng, MAP_ZOOM_LEVEL);
                mMap.animateCamera(cameraUpdate);
            }
        });
    }

    public void findPlace() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder
                    (PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException |
                GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
}
