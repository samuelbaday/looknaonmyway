package com.lookna.onmyway;

/**
 * Created by desktop1 on 10 Nov 2017.
 */
public class MyCustomObject {
    // Listener defined earlier
    public interface MyCustomObjectListener {
        void onHelloReady(String welcome);
    }

    private String customMessage = "";

    public void setCustomObjectListener(MyCustomObjectListener listener) {
        this.listener = listener;
        showWelcomeMessage();
    }

    private MyCustomObjectListener listener;

    public MyCustomObject() {
        this.listener = null;
    }

    public MyCustomObject(String customMessage,MyCustomObjectListener listener) {
        this.listener = listener;
        this.customMessage = customMessage;
        showWelcomeMessage();
    }

    public void showWelcomeMessage() {
        listener.onHelloReady("HELLO WORLD " + customMessage);
    }
}
