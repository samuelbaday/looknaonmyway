package com.lookna.onmyway;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class InterfaceExperimentActivity extends AppCompatActivity {
    private final String HELLO_TAG = "RESULT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interface_experiment);

        new MyCustomObject("It's me", new MyCustomObject.MyCustomObjectListener() {
            @Override
            public void onHelloReady(String welcome) {
                Toast.makeText(InterfaceExperimentActivity.this,welcome,Toast.LENGTH_SHORT).show();
                Log.i(HELLO_TAG,"RESULT: " + welcome);
            }
        });

    }
}
