package com.lookna.onmyway;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by desktop1 on 14 Nov 2017.
 */
public class LocationInputterService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private static GoogleApiClient mGoogleApiClient;
    private static LocationRequest mLocationRequest;
    private static Double latitude = 0.0,longitude = 0.0;

    private static List<LatLng> accumulatedCoordinates = new ArrayList<LatLng>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        buildGoogleApiClient();
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(ModeSetterClass.getIntervalTime());
        mLocationRequest.setFastestInterval(ModeSetterClass.getIntervalTime());
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("LOCATION","Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        upateMainMap();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void upateMainMap() {
        Log.d("BROADCAST", "Broadcasting message");
        Intent intent = new Intent(getString(R.string.broadcast_filter));
        // You can also include some extra new Data().
        intent.putExtra(getString(R.string.broadcast_tag), getString(R.string.broadcast_message));
        intent.putExtra(getString(R.string.broadcast_lat),latitude);
        intent.putExtra(getString(R.string.broadcast_lng),longitude);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
